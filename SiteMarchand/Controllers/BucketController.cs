﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteMarchand.Models;

namespace SiteMarchand.Controllers
{
    public class BucketController : Controller
    {
        private Classique_WebEntities1 db = new Classique_WebEntities1();
        List<Item> bucket;

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult AddtoBucket(int id, string url)
        {
            if (url != null)
            {
                Session["url"] = url;
            }
            
            if (Session["bucket"] == null)
            {
                bucket = new List<Item>();
            }
            else
            {
                bucket = (List<Item>)Session["bucket"];    
            }
            bucket.Add(new Item(db.Enregistrement.Find(id), 1));
            Session["bucket"] = bucket;
            return View("Index");
        }
    }
}