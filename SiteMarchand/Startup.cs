﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SiteMarchand.Startup))]
namespace SiteMarchand
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
